all: initrd.img
	@echo == final sizes ==
	@wc -c init initrd.img | head -n2

init: init.asm
	nasm -f bin -o $@ $<
	chmod +x $@

clean:
	rm -rf init initrd.img a.out initrd/

initrd.img: init
	mkdir -p initrd
	cp $< initrd/
	./mkinitrd initrd $@
	rm -rf initrd
