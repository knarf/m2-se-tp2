 [map all prog.map]
;; $ nasm -f bin prog.asm

BITS 32
        org 0x00200000

ehdr:               ; Elf32_Ehdr
        db   0x7F, "ELF", 1, 1, 1, 0  ; e_ident
_start:
        mov ecx, msg1
        inc ebx
        jmp _next


        dw    2                       ; e_type
        dw    3                       ; e_machine
        dd    1                       ; e_version
        dd    _start                  ; e_entry
        dd    phdr - $$               ; e_phoff
phdr:   dd      1                       ; e_shoff       ; p_type
        dd      0                       ; e_flags       ; p_offset
        dd      $$                      ; e_ehsize      ; p_vaddr
                                        ; e_phentsize
        dw      1                       ; e_phnum       ; p_paddr
        dw      0                       ; e_shentsize
        dd      filesize                ; e_shnum       ; p_filesz
                                        ; e_shstrndx
        dd      filesize                                ; p_memsz
        dd      5                                       ; p_flags
        dd      0x1000                                  ; p_align

_next:
    ;; write(1, msg1, len1)
    mov al, 4
    mov dl, len1
    int 80h

    ;; while (1) {
loop:

    ;; read(0, esp, 255)

    mov al, 3
    dec ebx
    mov ecx, esp
    not dl
    int 80h

    dec eax
    xchg eax, edi

    ;; write(1, msg2, len2)
    mov al, 4
    inc ebx
    mov ecx, msg2
    mov dl, len2
    int 80h

    ;; write(1, esp, dl)
    mov al, 4
    mov ecx, esp
    mov edx, edi
    int 80h

    ;; write(1, msg3, len3)
    mov al, 4
    mov ecx, msg3
    mov dl, len3
    int 80h
    jmp loop
    ;; }

msg1: db 'Say something and then press <Enter> :',0x0A
len1: equ $-msg1
msg2: db 'What you mean by ',
len2: equ $-msg2+1
msg3: db 39,'? '
len3: equ $-msg3

filesize   equ   $ - $$
